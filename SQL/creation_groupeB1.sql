/*
FIchier : Creation_GroupeA.sql
Auteurs : 
Vincent Campo 21605151 Gr.A
Yasmine Smail 21712908 Gr.A
Nom du groupe : B1
*/
CREATE TABLE YV_ACCESS(
  privilege VARCHAR(100) NOT NULL,
  deleteUser INTEGER NOT NULL,
  createDeleteTheme INTEGER NOT NULL,
  deleteOthersEvents INTEGER NOT NULL,
  createDeleteEvent INTEGER NOT NULL,
  subUnsubEvent INTEGER NOT NULL,
  changeOthersPrivilege INTEGER NOT NULL,
  CONSTRAINT accessPrivilegePk PRIMARY KEY(privilege)
);


CREATE TABLE YV_USERS(
name VARCHAR(100),
pwd VARCHAR(100),
privilege VARCHAR(100) NOT NULL,
age INTEGER,
mail VARCHAR(100),
registrationDay DATE,
CONSTRAINT usersNamePk PRIMARY KEY(name),
CONSTRAINT usersPrivilegeFk FOREIGN KEY(privilege) references YV_ACCESS(privilege));


CREATE TABLE YV_THEMES(
  theme VARCHAR(100),
  CONSTRAINT themePk PRIMARY KEY(theme)
);

CREATE TABLE YV_EVENTS(
  id INTEGER,
  author VARCHAR(100),
  theme VARCHAR(100) NOT NULL,
  name VARCHAR(100) NOT NULL,
  day DATE NOT NULL,
  description VARCHAR(100) NOT NULL,
  maxEffective INTEGER,
  minEffective INTEGER,
  effective INTEGER,
  address VARCHAR(100) NOT NULL,
  latitude INTEGER,
  longitude INTEGER,
  CONSTRAINT eventsIdPk PRIMARY KEY(id),
  CONSTRAINT eventsThemeFk FOREIGN KEY(theme) references YV_THEMES(theme),
  CONSTRAINT eventsAuthorFk FOREIGN KEY(author) references YV_USERS(name)	
);

CREATE TABLE YV_USERSEVENTS(
  name VARCHAR(100),
  eventId INTEGER,
  CONSTRAINT usersEventsUidFk FOREIGN KEY(name) references YV_USERS(name) ON DELETE CASCADE,
  CONSTRAINT usersEventsEidFk FOREIGN KEY(eventId) references YV_EVENTS(id) ON DELETE CASCADE
);


CREATE TABLE YV_MESSAGES(
  id INTEGER,
  title VARCHAR(100) NOT NULL,	
  message VARCHAR(500) NOT NULL,
  author VARCHAR(100),
  receiver VARCHAR(100),
  CONSTRAINT messageIdPk PRIMARY KEY(id),
  CONSTRAINT messageAuthorFk FOREIGN KEY(author) references YV_USERS(name) ON DELETE CASCADE,
  CONSTRAINT messageReceiverFk FOREIGN KEY(receiver) references YV_USERS(name) ON DELETE CASCADE
);
/* ~~~~~ trigger n�1~~~~~
 si un �v�nement existe � une distance de 1km la m�me date
  on envoie un message aux admins Les admins peuvent ensuite v�rifier si les deux �vents sont diff�rents.
 
*/
DELIMITER //

CREATE TRIGGER EVENTEXISTS AFTER INSERT ON YV_EVENTS
       FOR EACH ROW	  
BEGIN
	DECLARE	event INTEGER DEFAULT 0;
	DECLARE eventCount INTEGER DEFAULT 0;
	DECLARE finished INTEGER DEFAULT 0;
	DECLARE ad VARCHAR(100);
	DECLARE adCursor cursor for (SELECT name FROM YV_USERS WHERE privilege='ADMIN');
	DECLARE CONTINUE HANDLER FOR NOT FOUND SET finished = 1;
	-- s�lection de l'�v�nements proches et le m�me jour que celui ins�r�
	SELECT id,count(*) into event,eventCount FROM YV_EVENTS WHERE
	ABS(latitude-new.latitude)<=100 AND
	ABS(longitude-new.longitude)<=100 AND
	day=new.day AND id!=new.id limit 1;
	-- si on a trouv� un tel �v�nement on demande aux administrateurs de v�rifier
	-- en leur envoyant un message.
	IF(eventCount !=0) THEN
		OPEN adCursor;
		admin_loop : LOOP
		   FETCH adCursor INTO ad;
		   IF finished = 1 THEN
		      LEAVE admin_loop;
		   END IF;
		   INSERT INTO YV_MESSAGES VALUES
		       (RAND()*(99999-0)+0,'Evenement en attente de v�rification',
		       CONCAT(CONCAT(CONCAT(CONCAT('Deux �v�nements cr��s le m�me jour au m�me endroit.</br>Lien des �v�nements: <a href="evenement.php?eventid=', new.id),
			   '">Cliquer ici </a><a href="evenement.php?eventid='),event),'">Cliquer ici </a>'),'vincent',ad);
		end LOOP admin_loop;
	END IF;
END //

/* ~~~~~trigger n�2~~~~~
 Si un utilisateur s'inscrit � deux �venement � la m�me date  on envoie un message 
 � l'utilisateur lui demandant de choisir l'un des deux.
 */
 


CREATE TRIGGER SUBSAMEDAY BEFORE INSERT ON YV_USERSEVENTS
       FOR EACH ROW	  
BEGIN
       DECLARE subSameDay INTEGER;
       DECLARE newEventDay DATE;
	-- s�lection du jour du nouvel �v�nement
	SELECT day into newEventDay FROM YV_EVENTS WHERE id=new.eventId;
	-- s�lection des �v�nements inscrits le m�me jour que celui ins�r�
	SELECT id into subSameDay FROM YV_USERSEVENTS as UE
	JOIN YV_EVENTS E ON E.id=eventId
	WHERE UE.name=new.name AND E.day=newEventDay AND UE.eventId!=new.eventId limit 1;
	-- si on a trouv� un tel �v�nement on demande � l'utilisateur de choisir
	-- en lui envoyant un message.
	IF(subSameDay IS NOT NULL) THEN
		INSERT INTO YV_MESSAGES VALUES
		    (RAND()*(99999-0)+0,'Inscription le m�me jour',
		   CONCAT(CONCAT(CONCAT(CONCAT('Vous vous �tes inscrit � deux �v�nements la m�me d�te.</br>Lien des �v�nements: <a href="evenement.php?eventid=', new.eventId),
			   '">Cliquer ici </a><a href="evenement.php?eventid='),subSameDay),'">Cliquer ici </a>'),'vincent',new.name);
	END IF;
END //	
/*~~~~~trigger n�3~~~~~
	Si l'auteur d'un �v�nement modifie son �venement on envoie un lien vers
	l'�v�nement par message � tous ceux qui y sont inscrits
*/
CREATE TRIGGER MODIFIEREVENT AFTER UPDATE 
	ON YV_EVENTS FOR EACH ROW	  
BEGIN
	DECLARE finished INTEGER DEFAULT 0;
	DECLARE sub VARCHAR(100);
	DECLARE subsCursor cursor for (SELECT name FROM YV_USERSEVENTS WHERE eventId=new.id);
	DECLARE CONTINUE HANDLER FOR NOT FOUND SET finished = 1;

	-- si on a trouv� un tel �v�nement on demande aux administrateurs de v�rifier
	-- en leur envoyant un message.
		OPEN subsCursor;
		subs_loop : LOOP
		   FETCH subsCursor INTO sub;
		   IF finished = 1 THEN
		      LEAVE subs_loop;
		   END IF;
		   INSERT INTO YV_MESSAGES VALUES
		       (RAND()*(99999-0)+0,'Modification d\'un �v�nement',
			   CONCAT(CONCAT('Un �v�nement auquel vous �tes inscrits vien d\'�tre �dit�.</br>Lien de l\'�v�nement: <a href="evenement.php?eventid=', new.id),'">Cliquer ici </a>'),'vincent',sub);
		end LOOP subs_loop;
END //	

/* A chaque connexion d'un utilisateur on regarde s'il existe un utilisateur avec le privil�ge restreint
   et s'�tant inscrit il y a plus de 15 jour si on en trouve on passe son privilege � NORMAL*/
CREATE PROCEDURE FIFTEENDAYCHECK ()  
BEGIN
	DECLARE finished INTEGER DEFAULT 0;
	DECLARE usr VARCHAR(100);
	DECLARE usrCursor cursor for (SELECT name FROM YV_USERS WHERE DATEDIFF(CURDATE(),registrationDay)>15 AND privilege='RESTREINT');
	DECLARE CONTINUE HANDLER FOR NOT FOUND SET finished = 1;
	
	OPEN usrCursor;
		usrsLoop : LOOP
		   FETCH usrCursor INTO usr;
		   IF finished = 1 THEN
		      LEAVE usrsLoop;
		   END IF;
		   UPDATE YV_USERS SET privilege = 'NORMAL' WHERE name=usr;
		end LOOP usrsLoop;
END // 

/* Pour supprimer un privilege on enl�ve d'abord ce privilege � tous les utilisateur */
                                        
CREATE PROCEDURE DELETEPRIVILEGE (curPrivilege VARCHAR(100), newPrivilege VARCHAR(100))  
BEGIN

    UPDATE YV_USERS SET privilege=newPrivilege WHERE privilege=curPrivilege; 
	DELETE FROM YV_ACCESS WHERE privilege=curPrivilege;
END //


/* Pour supprimer un theme on enl�ve d'abord ce theme � tous les �v�nements */

CREATE PROCEDURE DELETETHEME (curTheme VARCHAR(100), newTheme VARCHAR(100))  
BEGIN
    UPDATE YV_EVENTS SET theme=newTheme WHERE theme=curTheme; 
	DELETE FROM YV_THEMES WHERE theme=curTheme;
END //
/* Pour supprimer un �v�nemenent on supprimes d'abord les inscriptions � cet �v�nement */



CREATE PROCEDURE DELETEEVENT (eId INT)  
BEGIN
    DELETE FROM YV_USERSEVENTS WHERE eventId=eId;
	DELETE FROM YV_EVENTS WHERE id=eId;
END //
DELIMITER ;

