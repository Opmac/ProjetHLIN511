/*
FIchier : test_groupeB1.sql
Auteurs : 
Vincent Campo 21605151 Gr.A
Yasmine Smail 21712908 Gr.A
Nom du groupe : B1
*/
insert into YV_ACCESS values('ADMIN',1,1,1,1,1,1);
insert into YV_ACCESS values('MODERATEUR',0,1,1,1,1,1);
insert into YV_ACCESS values('NORMAL',0,0,1,1,0,0);
insert into YV_ACCESS values('RESTREINT',0,0,0,1,0,0);
insert into YV_THEMES values('divers');/* theme obligatoire attribu� � tout �vent sans theme */
insert into YV_USERS values('vincent','azerty','ADMIN',20,'vincent@mail.fr','2019-12-18');
insert into YV_USERS values('yasmine','azerty','ADMIN',20,'yasmine@mail.fr','2019-12-18');

/* (triggers n�1)Cr�ation de deux �v�nements le m�me jour � +- 100 de distance 
   -> tous les admins recevrons un messages contenant les liens 
   des deux �vents et leur demandant le v�rifier s'il ne s'agit
   pas des m�mes �vents */
   /* */
insert into YV_EVENTS values(24567,'vincent','divers','R�union de no�l','2019-12-24','Distribution de capeaux et repas gratuit.',12,0,0,'13 rue des templiers 34123 Montpellier',1235,2435);
insert into YV_EVENTS values(24568,'yasmine','divers','Repas de no�l','2019-12-24','-distribution de cadeaux pour les enfants.</br>-repas gratuit.',12,0,0,'13 rue des templiers 34123 Montpellier',1200,2400);

/* (triggers n�2)Inscription � deux �v�nements le m�me jour,
    on envoie le lien des deux �v�nements par message en demendant � l'utilsateur
    de v�rifier	*/

insert into YV_USERSEVENTS values('vincent',24567);
insert into YV_USERSEVENTS values('vincent',24568);

/* (triggers n�3)L'auteur de l'�vent 24567(id) a modifi� son �v�nement 
    tous les utilisateurs inscrits � cet �v�nement(dont vincent) vont
    recevoir par message le lien de l'�v�nement modifier et leur 
    demendant de v�rifier	*/
	
UPDATE YV_EVENTS SET day='2019-12-25' WHERE id=24567;

