<!doctype html>
<html lang="fr">
<head>
<meta charset="utf8" />
<link rel="stylesheet" href="theme.css"/>
<title>tp4</title>
</head>
<body>
  <h2>Evènements</h2>

  <?php
 	require 'conf.php';
	try {
		
		if($privileges['createDeleteEvent']){
			if(isset($_POST['ajouterEvent']) && $_POST['ajouterEvent']==1) {
				
				print_r('<h2>Ajouter un évènement: </h2>
						<form action="evenement.php" method="post">');
						
				if(formComplete('ajouterEventForm',array('theme','nom','date','description','adresse','latitude','longitude'))){
					$id = hexdec( substr(uniqid(),-5) );
					$theme = htmlspecialchars($_POST['theme']);
					$nom = htmlspecialchars($_POST['nom']);
					$jour = htmlspecialchars($_POST['date']);
					$description = htmlspecialchars($_POST['description']);
					if(isset($_POST['effectifMax']) && isset($_POST['effectifMin'])) {
						$effectifMax = htmlspecialchars($_POST['effectifMax']);
						$effectifMin = htmlspecialchars($_POST['effectifMin']);
					} else {
						$effectifMax = -1;
						$effectifMin = -1;
					}
					$adresse = htmlspecialchars($_POST['adresse']);
					$latitude = htmlspecialchars($_POST['latitude']);
					$longitude = htmlspecialchars($_POST['longitude']);

					$themeExists = $dbh->query("SELECT count(*) AS count FROM YV_THEMES 	WHERE theme='".$theme."';")->fetch()[0];
					if($themeExists>0) {
						$ajouterEventQuery = $dbh
						->query("INSERT INTO YV_EVENTS VALUES('".$id."','".$_SESSION['username']."','".$theme."','".$nom."','".$jour."','".$description."','".$effectifMax."','".$effectifMin."',0,'".$adresse."','".$latitude."','".$longitude."');");
					} else {
						echo "Erreur: theme non existant.";
					}
				}
				print_r('
				    <p>
				      <label for="theme">Theme :</label>
				      <input type="text" name="theme" />
				    </p>
				    <p>
				      <label for="nom">Nom :</label>
				      <input type="text" name="nom" />
				    </p>
				    <p>
				      <label for="date">Date :</label>
				      <input type="date" name="date"/>
				    </p>
				    <p>
				      <label for="description">Description :</label>
				      <input type="text" name="description" />
				    </p>
				    <p>
				      <label for="effectifMax">Effectif maximum :</label>
				      <input type="number" name="effectifMax" />
				    </p>
				    <p>
				      <label for="effectifMin">Effectif minimum :</label>
				      <input type="number" name="effectifMin" />
				    </p>
				    <p>
				      <label for="adresse">Adresse :</label>
				      <input type="text" name="adresse" />
				    </p>
				    <p>
					<fieldset>
					<legend>Coordonnées</legend>
						<label for="latitude">Latitude :</label>
						<input type="number" name="latitude" />
						<label for="longitude">Longitude :</label>
						<input type="number" name="longitude" />
					</fieldset>
				    </p>
				    <input name="ajouterEvent" type="hidden" value="1">
				    <p>
				      <input type="submit" name="ajouterEventForm" value="Ajouter" /> <input type="reset" />
				    </p>
				  </form>');
				
			} else {
				print('<form action="evenement.php" method="post">
					    <p>
					    <input name="ajouterEvent" type="hidden" value="1">
					    <input type="submit" value="Ajouter un évènement"/>
					    </p>
					  </form>');
			}
		}
		if(isset($_GET['eventid'])) {
			$eventId = htmlspecialchars($_GET['eventid']);
			$event = $dbh
			->query("SELECT * FROM YV_EVENTS WHERE id=".$eventId.";")->fetch();
			$participants = $dbh
			->query("SELECT UE.name FROM YV_USERSEVENTS as UE JOIN YV_USERS  U ON U.name = UE.name WHERE eventId=".$eventId.";");
			
			if($isConnected) {
				$alreadySub = $dbh
					->query("SELECT count(*) AS count FROM YV_USERSEVENTS WHERE name='".$username."' AND eventId=".$eventId.";")->fetch();
				if(($privileges['createDeleteEvent']==1 && $event['author']==$username) 
					|| ($privileges['deleteOthersEvents']==1)) {
					print('<form action="#" method="post">
							<p>
							<input name="supprimer" type="hidden" value="1">
							<input type="submit" value="Supprimer mon évènement"/>
							</p>
						  </form>');
					print('<form action="#" method="post">
					    	<p>
							<input name="modifier" type="hidden" value="1">
							<input type="submit" value="Modifier mon évènement"/>
							</p>
						  </form>');
					#~~~~~~~~~~~~~~SUPRESSION D'EVENEMENT~~~~~~~~~~~~~~
					if(isset($_POST['supprimer']) && $_POST['supprimer']=1) {
						$dbh->query("CALL DELETEEVENT(".$eventId.");");
						header('Location: #');
					}
					#~~~~~~~~~~~MODIFICATION D'UN EVENEMENT~~~~~~~~~~~~
					if(isset($_POST['modifier']) && $_POST['modifier']=1) {
						print_r('<h2>Modifier mon évènement: </h2>
						<form action="evenement.php?eventid='.$eventId.'" method="post">');
						
						if(formComplete('modifierEventForm',array('theme','nom','date','description','adresse','latitude','longitude'))){
							$theme = htmlspecialchars($_POST['theme']);
							$nom = htmlspecialchars($_POST['nom']);
							$jour = htmlspecialchars($_POST['date']);
							$description = htmlspecialchars($_POST['description']);
							if(isset($_POST['effectifMax']) && isset($_POST['effectifMin'])) {
								$effectifMax = htmlspecialchars($_POST['effectifMax']);
								$effectifMin = htmlspecialchars($_POST['effectifMin']);
							} else {
								$effectifMax = -1;
								$effectifMin = -1;
							}
							$adresse = htmlspecialchars($_POST['adresse']);
							$latitude = htmlspecialchars($_POST['latitude']);
							$longitude = htmlspecialchars($_POST['longitude']);

							$themeExists = $dbh->query("SELECT count(*) AS count FROM YV_THEMES 	WHERE theme='".$theme."';")->fetch()[0];
							if($themeExists>0) {
								$ajouterEventQuery = $dbh
								->query("UPDATE YV_EVENTS SET theme='".$theme."',name='".$nom."',day='".$jour."',
								        description='".$description."',maxEffective='".$effectifMax."',
										minEffective='".$effectifMin."',address='".$adresse."',
										latitude='".$latitude."',longitude='".$longitude."'
										WHERE id=".$eventId.";");
							} else {
								echo "Erreur: theme non existant.";
							}
						}
						
						print_r('<p>
								  <label for="theme">Theme :</label>
								  <input type="text" name="theme" value="'.$event['theme'].'" />
								</p>
								<p>
								  <label for="nom">Nom :</label>
								  <input type="text" name="nom" value="'.$event['name'].'"/>
								</p>
								<p>
								  <label for="date">Date :</label>
								  <input type="date" name="date"value="'.$event['day'].'"/>
								</p>
								<p>
								  <label for="description">Description :</label>
								  <input type="text" name="description" value="'.$event['description'].'"/>
								</p>
								<p>
								  <label for="effectifMax">Effectif maximum :</label>
								  <input type="number" name="effectifMax" value="'.$event['maxEffective'].'"/>
								</p>
								<p>
								  <label for="effectifMin">Effectif minimum :</label>
								  <input type="number" name="effectifMin" value="'.$event['minEffective'].'"/>
								</p>
								<p>
								  <label for="adresse">Adresse :</label>
								  <input type="text" name="adresse" value="'.$event['address'].'"/>
								</p>
								<p>
								<fieldset>
								<legend>Coordonnées</legend>
									<label for="latitude">Latitude :</label>
									<input type="number" name="latitude" value="'.$event['latitude'].'"/>
									<label for="longitude">Longitude :</label>
									<input type="number" name="longitude" value="'.$event['longitude'].'"/>
								</fieldset>
								</p>
								</p>
									<input name="modifier" type="hidden" value="1">
								<p>
								<p>
								  <input type="submit" name="modifierEventForm" value="Modifier" /> <input type="reset" />
								</p>
							  </form>');
					}
				}
				if($alreadySub['count']>0){
					print('<form action="#" method="post">
						<p>
						<input name="desinscription" type="hidden" value="1">
						<input type="submit" value="Désinscrire"/>
						</p>
					  </form>');
					if(isset($_POST['desinscription']) && $_POST['desinscription']=1) {
						$dbh->query("DELETE FROM YV_USERSEVENTS WHERE name='".$username."' AND eventId=".$eventId.";");
						header('Location: #'); 
					}
				} else if($privileges['subUnsubEvent']){
					print('<form action="#" method="post">
						<p>
						<input name="inscription" type="hidden" value="1">
						<input type="submit" value="S\'inscrire"/>
						</p>
					  </form>');
					if(isset($_POST['inscription']) && $_POST['inscription']=1) {
						
							$dbh->query("INSERT INTO YV_USERSEVENTS VALUES('".$username."',".$eventId.")");
							header('Location: #'); 
					}
				}
			}
			print("<table>
				<tr><td>Auteur:</td><td> ".$event['author']."</td></tr>
				<tr><td>Nom:</td><td> ".$event['name']."</td></tr>
				<tr><td>Theme:</td><td> ".$event['theme']."</td></tr>
				<tr><td>Jour:</td><td> ".$event['day']."</td></tr>
				<tr><td>Description:</td><td> ".$event['description']."</td></tr>
				<tr><td>Effectif:</td><td> ".$participants->rowCount()."/".$event['maxEffective']."</td></tr>
				<tr><td>Adresse:</td><td> ".$event['address']."</td></tr>
				<tr><td>Coordonnée géographique:</td><td> ".$event['longitude']."/".$event['latitude']."</td></tr>
				<tr><td>Participants:</td><td> ");
			
			foreach($participants as $p){
				print_r($p['name']." ");
			}
			print_r('</td></tr></table>');
		} else {
			$themes = $dbh->query("SELECT * FROM YV_THEMES;");
			print('<form action="#" method="get">
				   <SELECT name="theme" size="1">');
						
			foreach ($themes as $t) {
				print('<OPTION>'.$t['theme']);
			}
			print('<OPTION>aucun</SELECT><input type="submit" value="Filtrer"/></form>');
			$event = "";
			if(isset($_GET['theme']) && $_GET['theme']!="aucun") {
				$theme = htmlspecialchars($_GET['theme']);
				$events = $dbh
				->query("SELECT * FROM YV_EVENTS WHERE theme='".$theme."';");
			} else {
				$events = $dbh
				->query("SELECT * FROM YV_EVENTS ORDER BY theme;");
			}
			echo '<table>';
			print_r('<tr>
						<td>Auteur</td>
			            <td>Nom</td>
			            <td>Date</td>
			            <td>Effectif</td>
			    		</tr>');
			foreach ($events as $e) {
				$participants = $dbh
				->query("SELECT UE.name FROM YV_USERSEVENTS as UE JOIN YV_USERS  U ON U.name = UE.name
						WHERE eventId=".$e['id'].";");

				print_r('<tr>
						<td>'.$e['author'].'</td>
			            <td><a href="evenement.php?eventid='.$e['id'].'"">'.$e['name'].'</a></td>
			            <td>'.$e['day'].'</td>
			            <td>'.$participants->rowCount().'/'.$e['maxEffective'].'</td>
			    		</tr>');
			}
			echo '</table>';
		}
	} catch(PDOException $e){
		echo $e->getMessage();
		die("Connexion impossible !");
	}



	?>
</body>
</html>
