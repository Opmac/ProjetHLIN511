<!DOCTYPE html>
<html>
<head>
	<title>Evenement</title>
	<link rel="stylesheet" type="text/css" href="theme.css">
</head>
<body id="acceuilBody">
	<?php 
		require'conf.php';
		#~~~~~~~~~~~~~~~~AFFICHAGE DES THEME~~~~~~~~~~~~~~~~
		$listeThemes = $dbh->query("SELECT theme FROM YV_THEMES;");
		print_r('<div class="boiteBody">');
		foreach($listeThemes as $t) {
			print_r('<a href="evenement.php?theme='.$t[0].'">'.$t[0].'</a></br>');
		}
		print_r('</div>');
		
		if($isConnected  ) {
			#~~~~~~~~~~~~~~~~AFFICHAGE DES PRIVILEGES~~~~~~~~~~
			$privlg = $dbh->query("SELECT * FROM YV_ACCESS;");
			$accessMysqlCol = $dbh->query("DESCRIBE YV_ACCESS;");
			$colLi = Array(); //liste des différents droits
			$i=0;
			while($c = $accessMysqlCol->fetch()) {
				if($i>0){
					array_push($colLi,$c[0]);
				}
				$i++;
			}
			print_r('<table><tr><td>Privilège</td>');
			foreach($colLi as $c) {
				print_r('<td>'.$c.'</td>');
			}
			print_r('</tr>');
			
			 foreach ($privlg as $e) {
				print_r('<tr><td>'.$e['privilege'].'</td>');
				$i =1;
				while(count($e)-1>$i) {
					print_r('<td>'.$e[$i].'</td>');
					$i++;
				}
				print_r('</tr>');
			}
			echo '</table>';
			if($privileges['createDeleteTheme']==1) {
				
				#~~~~~~~~~~~~~~~~AJOUT DE THEME~~~~~~~~~~~~~~~~
				print_r("<form action='#' method='post'><fieldset><legend>Ajouter un theme</legend>
						<label for='theme'>Theme</label>
						<input name='theme'/>
						<input type='submit' name='ajoutThemeForm' value='Ajouter'/><input type='submit' name='suppThemeForm' value='Supprimer'/></br>");
				if(formComplete('ajoutThemeForm',array('theme'))){
					$theme = htmlspecialchars($_POST['theme']);
					$dbh->query("INSERT INTO YV_THEMES VALUES('".$theme."');");
					header('Location: #');

				} else if(formComplete('suppThemeForm',array('theme'))){
					$theme = htmlspecialchars($_POST['theme']);
					$themeExists = $dbh->query("SELECT count(*) AS count FROM YV_THEMES	WHERE theme='".$theme."';")->fetch()[0];
					if($themeExists>0) {
						$dbh->query("CALL DELETETHEME('".$theme."','divers');");
						header('Location: #');
					} else {
						echo "Theme inexistant";
					}

				}
				print_r("</fieldset></form>");
			}	
			if($privileges['deleteUser']==1) {
				
				#~~~~~~~~~~~~~~~SUPPRESSION D'UTILISATEUR~~~~~~~~~~~~~~~~
				print_r("<form action='#' method='post'><fieldset><legend>Supprimer un utilisateur</legend>
						<label for='nom'>Utilisateur</label>
						<input name='nom'/>
						<input type='submit' name='supprimerUtilisateurForm' value='Supprimer'/></br>");
				if(formComplete('supprimerUtilisateurForm',array('nom'))){
					$utilisateurASup = htmlspecialchars($_POST['nom']);
					$dbh->query("DELETE FROM YV_USERS WHERE name='".$utilisateurASup."';");
					header('Location: #');

				}
				print_r("</fieldset></form>");
			}						
			if($privileges['changeOthersPrivilege']==1) {
			#~~~~~~~~~~~~~~~~ATTRIBUTION DE PRIVILEGE~~~~~~~~~~~~~~~~
				print_r("<form action='#' method='post'><fieldset><legend> Attribuer privilège</legend>
						<label for='privilege'>Privilege</label>
						<input name='privilege'/>
						<label for='Utilisateur'>Utilisateur</label>
						<input name='utilisateur'/>
						<input type='submit' name='attribPrivilegeForm' value='Ajouter'/></br>");
				if(formComplete('attribPrivilegeForm',array('utilisateur','privilege'))){
					$utilisateur = htmlspecialchars($_POST['utilisateur']);
					$privlg = htmlspecialchars($_POST['privilege']);
					if(privlg!="ADMIN" || $privileges['privilege']=="ADMIN") {
						$privilegeExists = $dbh->query("SELECT count(*) AS count 
														FROM YV_ACCESS	WHERE privilege='".$privlg."';")->fetch()[0];
						$utilisateurExists = $dbh->query("SELECT count(*) AS count 
														  FROM YV_USERS WHERE name='".$utilisateur."';")->fetch()[0];
						if($privilegeExists>0){
							if($utilisateurExists>0) {
								$currentPrivilege = $dbh->query("SELECT privilege 
																 FROM YV_USERS	WHERE name='".$utilisateur."';")->fetch()[0];
								if(($currentPrivilege != "ADMIN" && $currentPrivilege !="MODERATEUR") || 
								    $privileges['privilege']=="ADMIN") {
									$dbh->query("UPDATE YV_USERS SET privilege='".$privlg."' WHERE name='".$utilisateur."';");
									echo "Privilège attribué avec succès.";
									}else {echo "Seul un administrateur peut modifier
												 le privilège de modérateur/administrateur.";}
							} else {echo "Utilisateur inéxistant.";}
						} else {echo "Privilege inéxistant.";}
					} else {echo"Impossible d'attribuer le privilege administrateur sans être administrateur";}
				}
				print_r("</fieldset></form>");
			}
			if($privileges['privilege']=="ADMIN") {
				#~~~~~~~~~~~~~~~~SUPPRIMER PRIVILEGE~~~~~~~~~~~~~~~
				print_r("<form action='#' method='post'><fieldset><legend>(ADMIN) Supprimer un privilege</legend>
				<label for='privilege'>Privilege</label>
				<input name='privilege'/>
				<input type='submit' name='supprimerPrivilegeForm' value='Supprimer'/></br>");
				if(formComplete('supprimerPrivilegeForm',array('privilege'))){
					$privlg = htmlspecialchars($_POST['privilege']);
					$privilegeExists = $dbh->query("SELECT count(*) AS count FROM YV_ACCESS	WHERE privilege='".$privlg."';")->fetch()[0];
					if($privilegeExists>0) {
						$dbh->query("CALL DELETEPRIVILEGE('".$privlg."','NORMAL');");
						header('Location: #');						
					} else {
						print_r("Privilege inexistant.");
					}

				}
				print_r("</fieldset></form>");
				#~~~~~~~~~~~~~~~~CREATION DE PRIVILEGE~~~~~~~~~~~~~~~~
				print_r("<form action='#' method='post'><fieldset><legend>(ADMIN) Ajouter un Privilege</legend>
						<label for='privilege'>Privilege: </label>
						<input name='privilege'/>");
				foreach($colLi as $c) {
					print_r("
						<label for='".$c."'>".$c."</label>
						<input name='".$c."'/>");
				}
				if(isset($_POST['privilege'])){
					if(formComplete('ajouterPrivilegeForm',$c)){
						$privlg = htmlspecialchars($_POST['privilege']);
						$values = "";
						$i=0;
						foreach($colLi as $c) {
							$values = $values.htmlspecialchars($_POST[$c]);
							if($i!=count($colLi)-1) $values = $values.",";
							$i++;
						}
						
						$privilegeExists = $dbh->query("SELECT count(*) AS count FROM YV_ACCESS	WHERE privilege='".$privlg."';")->fetch()[0];
						if($privilegeExists>0) {
							print_r( "</br>Privilege déjà existant.");
						} else {
							$dbh->query("INSERT INTO YV_ACCESS VALUES('".$privlg."',".$values.");");
							header('Location: #');
						}
					}
				}
				print_r("<input type='submit' name='ajouterPrivilegeForm' value='Ajouter'/></br></fieldset></form>");
			}
		}
?>

	
	

</body>
</html>