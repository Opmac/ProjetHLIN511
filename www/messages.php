<!doctype html>
<html lang="fr">
<head>
<meta charset="utf8" />
<link rel="stylesheet" href="theme.css"/>
<title>tp4</title>
</head>
<body>
  <h2>Messages</h2>


	<?php
		require'conf.php';
		try {
			if($isConnected) {	
				print_r('<form action="#" method="post" id="textareamsg">');
				#~~~~~~~~~~~~ ENVOI DES MESSAGES~~~~~~~~~~~~
				if(formComplete("envoyerMsgForm",array('titre','dest','message'))) {
					$id = hexdec( substr(uniqid(),-5) );
					$message = htmlspecialchars($_POST['message']);
					$titre = htmlspecialchars($_POST['titre']);
					$dest = htmlspecialchars($_POST['dest']);
					$destExists = $dbh
						->query("SELECT count(*) FROM YV_USERS WHERE name='".$dest."';")
						->fetch()[0];
					if($destExists!=0) {
						$destExists = $dbh
						->query("INSERT INTO YV_MESSAGES VALUES('".$id."','".$titre."','".$message."','".$username."','".$dest."');");
					} else {
						echo "Utilisateur inexistant.";
					}
				}			
					print('
						<p>
	    				<label for="dest">Nom d\'utilisateur :</label>
	   					<input type="text" name="dest" />
	    				</p>
	    				<p>
	    				<label for="titre">Titre :</label>
	   					<input type="text" name="titre" />
	    				</p>
	   					<p>
						<label for="message">Message :</label>
	      				<textarea form="textareamsg" name="message" rows="5" cols="33">Entrez votre message.
						</textarea>
					    </p>
					    <p>
					      <input type="submit" name="envoyerMsgForm" value="Envoyer" /> <input type="reset" />
					    </p>
					  	</form>');
				#~~~~~~~~~~~~ SUPPRESSION DE MESSAGES~~~~~~~~~~~~
				if(isset($_GET['msgid'])) {
					$id = htmlspecialchars($_GET['msgid']);
					$msgExists = $dbh
						->query("SELECT count(*) FROM YV_MESSAGES WHERE receiver='".$username."' AND id='".$id."';")
						->fetch()[0];
					if($msgExists!=0) {
						$dbh
						->query("DELETE FROM YV_MESSAGES WHERE id='".$id."'");
					} else {
						echo "Message inexistant.";
					}
				}
				#~~~~~~~~~~~~ LISTE DES MESSAGES~~~~~~~~~~~~
				$message = $dbh
				->query("SELECT * FROM YV_MESSAGES WHERE receiver='".$username."';");
				echo '<table>';
				print_r('<tr>
							<td>Auteur</td>
				            <td>Titre</td>
				            <td>Message</td>
				    	</tr>');
				foreach ($message as $e) {
					print_r('<tr>
					<td>'.$e['author'].'</td>
				    <td>'.$e['title'].'</td>
				    <td>'.$e['message'].'</td>
					<td><a href="?msgid='.$e['id'].'">X</a></td>
					</tr>');
				}
				echo '</table>';
			}
		} catch(PDOException $e){
			echo $e->getMessage();
			die("Connexion impossible !");
		}

	?>

</body>
</html>
